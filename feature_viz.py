#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from sklearn import cluster
from pathlib import Path
import networkx as nx
from concepts import Context
import matplotlib.pyplot as plt
from itertools import combinations, groupby


def to_networkx(lattice, full_labels=False):
    """ Return a networkx graph representing the lattice.

    Args:
        lattice: a lattice object from concepts.
        full_labels: use full extent in labels rather than objects.

    Returns:
        networkx DiGraph object
    """

    def format(labels):
        if len(labels) > 0:
            return "{{{}}}".format(" ".join(labels))
        return ""

    def label(concept):
        if concept is lattice.infimum:
            return "{}"
        objs = format(concept.extent if full_labels else concept.objects)
        props = format(concept.properties)

        if not objs:
            return props
        if not props:
            return objs
        return props + "\n" + objs

    G = nx.DiGraph()
    for concept in lattice._concepts:
        i = concept.index
        G.add_node(i, label=label(concept))
        for neighbor in concept.lower_neighbors:
            j = neighbor.index
            G.add_edge(i, j)
    return G


def to_context(table):
    """ Return a context from a feature table.

    Args:
        table: feature table in the Qumin format.

    Returns:
        context object from concepts.
    """

    def features_names(columns):
        signs = ["-", "+"]
        colset = set(columns)
        for c in columns:
            key, val = c.split("=")
            val = int(float(val))
            if val < 2:
                yield signs[val] + key.replace(" ", "_")
            else:
                yield c.replace(" ", "_")

    context_table = pd.get_dummies(table, prefix_sep="=")
    context_table.columns = features_names(context_table.columns)
    context_table = context_table.applymap(lambda x: "X" if x == 1 else "")
    context_str = context_table.to_csv()
    return Context.fromstring(context_str, frmat='csv')


def features_from_file(filepath):
    """ Reading a Qumin format feature file.

    - The file can be csv or tsv
    - the expected encoding is utf-8
    - non specified values can be blank or "-1"
    - Any UNICODE, ALIAS or value columns will be ignored
    - Qumin class shorthands will be ignored

    Args:
        filepath: path of the feature file.

    Returns:
        a pandas dataframe representing the features.
    """
    with open(filepath, "r", encoding="utf-8") as f:
        first_line = f.readline()
        if "Seg.," in first_line or '"Seg.",' in first_line:
            sep = ","
        elif "Seg.\t" in first_line or '"Seg."\t' in first_line:
            sep = "\t"
    table = pd.read_table(filepath, header=0, index_col=0,
                          na_values=-1, sep=sep, encoding="utf-8")
    if table.index.has_duplicates:
        table = table[table.index.drop_duplicates(keep='first')]
    if table.index[0] == "Seg.":
        table.columns = table.iloc[0, :]
        table = table.iloc[1:, :]

    if "UNICODE" in table.columns:
        table.drop("UNICODE", axis=1, inplace=True)
    if "ALIAS" in table.columns:
        table.drop("ALIAS", axis=1, inplace=True)
    if "value" in table.columns:
        table.drop("value", axis=1, inplace=True)

    table = table.applymap(lambda x: None if pd.isna(x) else str(x))
    idx = [i for i in table.index if not (i.startswith("#") and i.endswith("#"))]
    return table.loc[idx, :]


def draw(graph, path=None):
    """Draw a graph as a phoneme lattice."""
    n = len(graph)
    size = (5, 5)
    if n > 15:
        size = (5 + n * 0.06, 5 + n * 0.06)

    fig = plt.figure(figsize=size)
    pos = nx.drawing.nx_agraph.graphviz_layout(graph, prog='dot')
    nx.draw_networkx_nodes(graph, pos, alpha=0)
    nx.draw_networkx_edges(graph, pos, arrows=False, width=.5)
    labels = nx.get_node_attributes(graph, 'label')
    label_text = nx.draw_networkx_labels(graph, labels=labels, pos=pos, font_size=5)
    for node in label_text:
        text = label_text[node]
        if text.get_text():
            # text.set_backgroundcolor("white")
            text.set_bbox({'facecolor': 'white', 'edgecolor': 'none', 'pad': 0, 'alpha': 1})
            text.set_linespacing(2)

    # Adjust margins for networkx labels out of frame
    x_values, y_values = zip(*pos.values())
    x_max = max(x_values)
    x_min = min(x_values)
    x_margin = (x_max - x_min) * 0.15
    plt.xlim(x_min - x_margin, x_max + x_margin)

    plt.gca().set_axis_off()
    plt.tight_layout(pad=1)
    if path is None:
        plt.show()
    else:
        fig.savefig(path, dpi=300, pad_inches=0)


def get_matrix(lattice, alphabet):
    """ Compute a dissimilarity matrix for a phoneme inventory.

    The dissimilarity between segments a and b is 1-sim(a,b).
    The similarity used is the jaccard index for natural classes, as proposed by Frisch (1996).

    Args:
        lattice: lattice object from concept representing the natural classes in the inventory.
        alphabet: list of phonemes in the inventory.

    Returns:
        dissimilarity matrix as a n*n array.
    """
    l = len(alphabet)
    sim_matrix = np.ones((l, l))
    classes = {}
    for i, a in enumerate(alphabet):
        ancestors = lattice[[a]].upset()
        classes[i] = set(
            "".join(sorted(ancestor.extent)) + "=" + "".join(sorted(ancestor.intent)) for ancestor in ancestors)
        classes[i].add(a)
    for i, j in combinations(range(l), 2):
        s = len(classes[i] & classes[j]) / len(classes[i] | classes[j])
        sim_matrix[i, j] = sim_matrix[j, i] = s
    return 1 - sim_matrix


def find_clusters(m, alphabet, n_per_cluster=5):
    """ Find clusters of phonemes, trying to honor a cluster size.

    Args:
        m: dissimilarity matrix
        alphabet: list of phonemes in the inventory.
        n_per_cluster: ideal number of phonemes per cluster

    Returns:
        A list of tuples `(i, p)`, where `p` is a phoneme and `i` a cluster number.
    """
    g_len = m.shape[0] // n_per_cluster
    clusters = cluster.AgglomerativeClustering(n_clusters=g_len, affinity='precomputed',
                                               linkage='complete').fit_predict(m)
    return sorted(zip(clusters, alphabet))

def check_unique_phoneme_definition(context, alphabet, strict=False):
    for sound in alphabet:
        lattice_node = "".join(sorted(context.lattice[[sound]].extent))
        if sound != lattice_node:
            print("Warning: the sound [{}] is underspecified and identical to the class [{}]".format(sound, lattice_node))
            print("Features for {}:{}".format(lattice_node, context.lattice[[sound]].intent))
            if strict:
                raise ValueError("incorrect phoneme definition")

def main(args):
    """Generate natural class lattices for phoneme inventories defined by distinctive features.
    This can be useful to visualize the natural classes implied by distinctive features.
    """
    feature_table = features_from_file(args.segments)
    context = to_context(feature_table)
    alphabet = list(feature_table.index)

    check_unique_phoneme_definition(context, alphabet)

    if args.output_dir is not None:
        directory = args.output_dir
    else:
        directory = Path(args.segments).parent / Path(Path(args.segments).stem + "_figures/")
    directory.mkdir(exist_ok=True)

    if args.select:
        print("Exporting lattice for: {}".format(args.select))
        group_table = feature_table.loc[args.select, :]
        group_context = to_context(group_table)
        full_extent = len(group_context.lattice) < 20
        G = to_networkx(group_context.lattice, full_labels=full_extent)
        draw(G, directory / "lattice_{}.png".format("-".join(args.select)))
    elif args.all or (args.number >= len(alphabet)):
        print("Exporting lattice for all phonemes")
        full_extent = len(context.lattice) < 20
        G = to_networkx(context.lattice, full_labels=full_extent)
        draw(G, directory / "full_lattice.png")
    else:
        dissimilarities = get_matrix(context.lattice, alphabet)
        clusters = find_clusters(dissimilarities, alphabet, args.number)
        for key, group in groupby(clusters, lambda x: x[0]):
            keys, group = zip(*group)
            index = context.lattice[group].extent  # normalize to nearest natural class
            print("Exporting lattice for: {}".format(index))
            group_table = feature_table.loc[index, :]
            group_context = to_context(group_table)
            full_extent = len(group_context.lattice) < 20
            G = to_networkx(group_context.lattice, full_labels=full_extent)
            draw(G, directory / "lattice_{}.png".format("-".join(index)))


if __name__ == '__main__':
    import argparse

    usage = main.__doc__

    parser = argparse.ArgumentParser(description=usage, formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("segments", help="Phonological segment file, full path, csv", type=str)
    parser.add_argument("-o", "--output_dir", help="output directory", type=Path, default=None)

    group = parser.add_mutually_exclusive_group()
    group.add_argument("-n", "--number", help="approximate number of phonemes per sub-lattice", type=int, default=5)
    group.add_argument("-s", "--select", help="select a few phonemes to export (space separated)", nargs="+", type=str,
                       default=[])
    group.add_argument("-a", "--all", help="export only the complete lattice", action="store_true")
    args = parser.parse_args()
    main(args)
