This script can be used to visualize natural classes in phoneme inventories, as defined by distinctive features.
It can be used to check how choices of distinctive features impact the structure of natural classes.

The expected format for the phonological segments is the same as that of the [segments file for Qumin](https://qumin.readthedocs.io/segments.html). 
Any shorthands rows, as well as Qumin specific columns (aliases, values) will be ignored. An example is given in the file `V_example.csv`.

Example input:

    python3 feature_viz.py -o ./ -- V_example.csv 

Example output:

![example lattice for vowels](full_lattice.png)

The lattices are generated using the excellent package for Formal Concept Analysis in python: [concepts](https://pypi.org/project/concepts/).

There are three main ways of generating lattices:

- the option `--all` outputs an overview of the structure for the entire inventory. This is usually too large to be readable.
- Using the option `--select` followed by a (space separated) sequence of phonemes produces a lattice for that exact set of phonemes.
- Otherwise, the program will cluster phonemes according to their similarity as defined by the features, and export a sub-lattice for each cluster. 
The optimal size of clusters can be adjusted using the parameter `--number`, which specifies an ideal number of phonemes per cluster. 
If a cluster does not constitute a natural class, the generated sublattice will be that to the nearest natural class. 
This ensures that the clustering does not generate false nodes. As a result, there can be duplicated phonemes across sublattices.

# Usage

    feature_viz.py [-h] [-o OUTPUT_DIR] [-n NUMBER | -s SELECT [SELECT ...] | -a] segments

positional arguments:
~~~
  segments              Phonological segment file, full path, csv
~~~
optional arguments:
~~~
  -h, --help            show this help message and exit
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        output directory
  -n NUMBER, --number NUMBER
                        approximate number of phonemes per sub-lattice
  -s SELECT [SELECT ...], --select SELECT [SELECT ...]
                        select a few phonemes to export (space separated)
  -a, --all             export only the complete lattice
~~~

